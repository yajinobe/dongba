package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yajinobe
 */
@SpringBootApplication
public class DongbaApplication {

    public static void main(String[] args) {
        SpringApplication.run(DongbaApplication.class, args);
    }

}
