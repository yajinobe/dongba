package com.cy.pj.common.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author 基于此对象封装数据层返回的数据以及计算的分页信息
 * @param <T>
 */
@Accessors(chain = true)
@NoArgsConstructor
@Data
public class PageObject<T> implements Serializable {
    private static final long serialVersionUID = 2768066434799380749L;
    private Integer pageCurrent = 1;
    private Integer pageSize = 3;
    private Integer rowCount = 0;
    private Integer pageCount = 0;
    /**当前页记录*/
    private List<T> records;
    public PageObject(Integer pageCurrent,Integer pageSize,Integer rowCount,List<T> records){
        this.pageCurrent = pageCurrent;
        this.pageSize = pageSize;
        this.rowCount = rowCount;
        this.records = records;
        this.pageCount = (rowCount-1)/pageSize+1;
    }
}
