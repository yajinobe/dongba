package com.cy.pj.common.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yajinobe
 */
@Data
public class Node implements Serializable {
    private static final long serialVersionUID = -8035928111049328300L;
    private Integer id;
    private String name;
    private Integer parentId;
}
