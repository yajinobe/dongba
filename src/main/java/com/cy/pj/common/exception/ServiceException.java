package com.cy.pj.common.exception;

/**
 * 自定义异常处理类
 * @author yajinobe
 */
public class ServiceException extends RuntimeException{
    private static final long serialVersionUID = 5843835376260549700L;
    public ServiceException(){
    }
    public ServiceException(String message){
        super(message);
    }
    public ServiceException(Throwable cause){
        super(cause);
    }
}
