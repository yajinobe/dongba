package com.cy.pj.common.aspect;

import com.cy.pj.common.annotation.RequiredLog;
import com.cy.pj.common.util.IPUtils;
import com.cy.pj.sys.entity.SysLog;
import com.cy.pj.sys.service.SysLogService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * 定义日志切面类对象，通过环绕通知处理日志操作记录
 * @author yajinobe
 */
@Aspect
@Component
@Slf4j
public class SysLogAspect {
    @Autowired
    private SysLogService sysLogService;

    @Pointcut("@annotation(com.cy.pj.common.annotation.RequiredLog)")
    public void logPointCut(){
    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable{
        long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long endTime = System.currentTimeMillis();
        long totalTime = endTime-startTime;
        log.info("方法执行的总时长为："+totalTime);
        saveSysLog(joinPoint,totalTime);
        return result;
    }

    private void saveSysLog(ProceedingJoinPoint point,long totalTime) throws NoSuchMethodException, JsonProcessingException {
        //1.获取日志信息
        MethodSignature ms = (MethodSignature) point.getSignature();
        //getTarget():获取当前对象
        Class<?> targetClass = point.getTarget().getClass();
        String className = targetClass.getName();
        //获取方法名 参数类型
        String methodName = ms.getMethod().getName();
        Class<?>[] parameterTypes = ms.getMethod().getParameterTypes();
        //获取目标方法对象
        Method declaredMethod = targetClass.getDeclaredMethod(methodName, parameterTypes);
        String username="admin";
        Object[] paramsObj = point.getArgs();
        String params = new ObjectMapper().writeValueAsString(paramsObj);
        System.out.println("paramsObj:"+params);
        RequiredLog requiredLog = declaredMethod.getDeclaredAnnotation(RequiredLog.class);
        //封装日志对象
        SysLog sysLog = new SysLog();
        if(requiredLog!=null){
            sysLog.setOperation(requiredLog.value());
        }
        sysLog.setUsername(username).setMethod(className+"."+methodName).setParams(params).setIp(IPUtils.getIpAddr())
                .setTime(totalTime).setCreatedTime(new Date());
        sysLogService.saveObject(sysLog);
    }
}
