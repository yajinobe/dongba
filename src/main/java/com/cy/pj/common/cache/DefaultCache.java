package com.cy.pj.common.cache;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Lazy //延迟加载，通常配合单例作用域使用
@Scope("singleton") //prototype表示每次创建一个新的对象
@Component
public class DefaultCache implements Cache{
    public DefaultCache(){
        System.out.println("cache()");
    }

    @PostConstruct //执行构造方法之后执行
    public void init(){
        System.out.println("init()");
    }

    @PreDestroy //对象销毁之前执行
    public void destroy(){
        System.out.println("destroy()");
    }
}
