package com.cy.pj.common.web;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author yajinobe
 */
@NoArgsConstructor
@Data
public class JsonResult implements Serializable {
    private static final long serialVersionUID = -8774711588216992602L;
    /**状态码 1表示success，0表示error*/
    private int state = 1;
    /**状态信息*/
    private String message = "ok";
    /**正确数据*/
    private Object data;

    /**
     * 一般查询时调用，封装查询结果
     * @param data 封装数据
     */
    public JsonResult(Object data) {
        this.data = data;
    }
    public JsonResult(String message) {
        this.message = message;
    }

    /**
     * 出现异常时调用
     * @param t 异常对象
     */
    public JsonResult(Throwable t) {
        this.state = 2;
        this.message = t.getMessage();
    }
}
