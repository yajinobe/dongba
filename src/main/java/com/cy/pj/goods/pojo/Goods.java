package com.cy.pj.goods.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Goods {
    private Long id;
    private String name;
    private String remark;
    private Date createdTime;
}
