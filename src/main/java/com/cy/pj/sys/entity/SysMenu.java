package com.cy.pj.sys.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yajinobe
 */
@Data
public class SysMenu implements Serializable {
    private static final long serialVersionUID = 6346395113389356124L;
    private Integer id;
    private String name;
    private String url;
    /**菜单类型（按钮和普通菜单）*/
    private Integer type=1;
    private Integer sort;
    private String note;
    private Integer parentId;
    private String permission;
    private String createdUser;
    private String modifiedUser;
    private Date createdTime;
    private Date modifiedDate;
}
