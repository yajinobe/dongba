package com.cy.pj.sys.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yajinobe
 * CREATE TABLE `sys_logs` (
 *   `id` bigint(20) NOT NULL AUTO_INCREMENT,
 *   `username` varchar(50) DEFAULT NULL COMMENT '用户名',
 *   `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
 *   `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
 *   `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
 *   `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
 *   `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
 *   `createdTime` datetime DEFAULT NULL COMMENT '创建时间',
 *   PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='系统日志';
 */
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SysLog implements Serializable {
    private static final long serialVersionUID = 831521886501928675L;
    private Integer id;
    private String username;
    private String operation;
    private String method;
    private String params;
    /**执行时长*/
    private Long time;
    private String ip;
    private Date createdTime;
}
