package com.cy.pj.sys.entity;

import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 角色对象
 * @author yajinobe
 */
@Data
public class SysRole implements Serializable {
    private static final long serialVersionUID = -8680185648041885009L;
    private Integer id;
    private String name;
    private String note;
    private Date createdTime;
    private String createdUser;
    private String modifiedUser;
}
