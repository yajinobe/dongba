package com.cy.pj.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cy.pj.sys.entity.SysLog;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * 日志对象接口
 * @author yajinobe
 */
@Mapper
public interface SysLogDao {
    /**
     * 根据用户名查询总记录数
     * @param username 用户名
     * @return 总记录数
     */
    int getRowCount(String username);

    /**
     * 查询指定页的记录
     * @param username 用户名
     * @param startIndex 当前页的起始位置
     * @param pageSize 页面大小
     * @return 当前页的日志记录信息
     */
    List<SysLog> findPageObjects(String username,Integer startIndex,Integer pageSize);

    /**
     * 删除选中日志
     * @param ids 选中的所有日志id
     * @return 影响行数
     */
    int deleteObjects(Integer[] ids);

    /**
     * 添加日志，实现日志信息的持久化
     * @param sysLog 日志对象
     * @return 影响行数
     */
    int insertObject(SysLog sysLog);
}
