package com.cy.pj.sys.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户角色关系
 * @author yajinobe
 */
@Mapper
public interface SysUserRoleDao {
    /**
     * 基于角色id删除用户角色关系
     * @param roleId 角色id
     * @return 影响行数
     */
    @Delete("delete from sys_user_roles where role_id=#{roleId}")
    int deleteObjectsByRoleId(Integer roleId);
}
