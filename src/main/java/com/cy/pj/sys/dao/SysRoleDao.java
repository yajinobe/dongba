package com.cy.pj.sys.dao;

import com.cy.pj.sys.entity.SysRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author yajinobe
 */
@Mapper
public interface SysRoleDao {
    /**
     * 按条件统计记录总数
     * @param name 角色名
     * @return 总记录数
     */
    int getRowCount(String name);

    /**
     * 查询当前页的记录
     * @param name 角色名
     * @param startIndex 开始位置
     * @param pageSize 页面大小
     * @return 当前页记录
     */
    List<SysRole> findPageObjects(String name,Integer startIndex,Integer pageSize);

    /**
     * 删除角色
     * @param id 角色id
     * @return 影响行数
     */
    @Delete("delete from sys_roles where id=#{id}")
    int deleteObject(Integer id);
}
