package com.cy.pj.sys.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色菜单关系
 * @author yajinobe
 */
@Mapper
public interface SysRoleMenuDao {
    /**
     * 删除角色菜单关系
     * @param menuId 参数
     * @return 影响行数
     */
    @Delete("delete from sys_role_menus where menu_id=#{menuId}")
    int deleteObjectsByMenuId(Integer menuId);

    /**
     * 基于角色id删除角色菜单关系
     * @param roleId 角色id
     * @return 影响行数
     */
    @Delete("delete from sys_role_menus where role_id=#{roleId}")
    int deleteObjectsByRoleId(Integer roleId);
}
