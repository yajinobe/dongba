package com.cy.pj.sys.dao;

import com.cy.pj.common.vo.Node;
import com.cy.pj.sys.entity.SysMenu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import java.util.List;
import java.util.Map;

/**
 * 菜单接口
 * @author yajinobe
 */
@Mapper
public interface SysMenuDao{
    /**
     * 将用户提交的菜单数据，持久化带数据库
     * @param sysMenu 菜单对象
     * @return 影响行数
     */
    int insertObject(SysMenu sysMenu);

    /**
     * 查询菜单以及上级菜单
     * @return 返回查询数据，一个map封装一行记录
     */
    List<Map<String,Object>> findObjects();

    /**
     * 查询上级菜单相关信息
     * @return 查询结果
     */
    @Select("select id,name,parentId from sys_menus")
    List<Node> findZtreeMenuNodes();

    /**
     * 基于菜单id查询子菜单记录的方法
     * @param id 菜单id
     * @return 子菜单数量
     */
    @Select("select count(*) from sys_menus where parentId=#{id}")
    int getChildCount(Integer id);

    /**
     * 根据菜单id删除菜单
     * @param menuId 菜单id
     * @return  影响行数
     */
    @Delete("delete from sys_menus where id=#{id}")
    int deleteObject(Integer menuId);

    /**
     * 更新菜单
     * @param sysMenu 更新后的菜单对象
     * @return 影响行数
     */
    int updateObject(SysMenu sysMenu);
}
