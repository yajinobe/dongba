package com.cy.pj.sys.service.impl;

import com.cy.pj.common.annotation.RequiredLog;
import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.vo.Node;
import com.cy.pj.sys.dao.SysMenuDao;
import com.cy.pj.sys.dao.SysRoleMenuDao;
import com.cy.pj.sys.entity.SysMenu;
import com.cy.pj.sys.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * @author yajinobe
 */
@Service
public class SysMenuServiceImpl implements SysMenuService {
    @Autowired
    private SysMenuDao sysMenuDao;
    @Autowired
    private SysRoleMenuDao sysRoleMenuDao;

    @Override
    public List<Map<String, Object>> findObjects() {
        List<Map<String, Object>> list = sysMenuDao.findObjects();
        if(list==null||list.size()==0){
            throw new ServiceException("没有对应的菜单信息");
        }
        return list;
    }

    @Override
    public List<Node> findZtreeMenuNodes() {
        return sysMenuDao.findZtreeMenuNodes();
    }

    @Override
    public int deleteObject(Integer id) {
        //1.验证数据合法性
        if(id==null||id<0){
            throw new IllegalArgumentException("请先选择");
        }
        //2.获取子菜单数量
        int count = sysMenuDao.getChildCount(id);
        if(count>0){
            throw new ServiceException("请先删除子菜单");
        }
        //3.删除角色菜单关系
        sysRoleMenuDao.deleteObjectsByMenuId(id);
        //4.删除菜单元素
        int rows = sysMenuDao.deleteObject(id);
        if(rows==0){
            throw new ServiceException("此菜单可能已经不存在");
        }
        return rows;
    }

    @RequiredLog("saveObject")
    @Override
    public int saveObject(SysMenu sysMenu) {
        if(sysMenu==null){
            throw new ServiceException("保存对象不能为空");
        }
        if(StringUtils.isEmpty(sysMenu.getName())){
            throw new ServiceException("菜单名不能为空");
        }
        int rows;
        try {
            rows = sysMenuDao.insertObject(sysMenu);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("保存失败");
        }
        return rows;
    }

    @Override
    public int updateObject(SysMenu sysMenu) {
        if(sysMenu==null){
            throw new ServiceException("保存对象不能为空");
        }
        if(StringUtils.isEmpty(sysMenu.getName())){
            throw new ServiceException("菜单名不能为空");
        }
        int rows;
        try {
            rows = sysMenuDao.updateObject(sysMenu);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("更新失败");
        }
        if(rows==0){
            throw new ServiceException("记录可能已经不存在");
        }
        return rows;
    }
}
