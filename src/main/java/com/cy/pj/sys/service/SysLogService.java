package com.cy.pj.sys.service;

import com.cy.pj.common.vo.PageObject;
import com.cy.pj.sys.entity.SysLog;

/**
 * @author yajinobe
 */
public interface SysLogService {
    /**
     * 基于用户名和页码查询当前页记录和分页信息
     * @param name 查询条件
     * @param pageCurrent 当前页码
     * @return 当前页记录和分页信息
     */
    PageObject<SysLog> findPageObjects(String name,Integer pageCurrent);

    /**
     * 删除选中日志
     * @param ids 选中日志id
     * @return 影响行数
     */
    int deleteObjects(Integer[] ids);

    /**
     * 新增日志
     * @param sysLog 日志对象
     */
    void saveObject(SysLog sysLog);
}
