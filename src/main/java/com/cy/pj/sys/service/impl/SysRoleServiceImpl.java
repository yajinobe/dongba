package com.cy.pj.sys.service.impl;

import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.vo.PageObject;
import com.cy.pj.sys.dao.SysRoleDao;
import com.cy.pj.sys.dao.SysRoleMenuDao;
import com.cy.pj.sys.dao.SysUserRoleDao;
import com.cy.pj.sys.entity.SysRole;
import com.cy.pj.sys.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yajinobe
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    private SysRoleDao sysRoleDao;
    @Autowired
    private SysUserRoleDao sysUserRoleDao;
    @Autowired
    private SysRoleMenuDao sysRoleMenuDao;

    @Override
    public PageObject<SysRole> findPageObjects(String name, Integer pageCurrent) {
        //1.校验参数
        if(pageCurrent==null||pageCurrent<1){
            throw new ServiceException("当前页码值无效");
        }
        int rowCount = sysRoleDao.getRowCount(name);
        if(rowCount==0){
            throw new ServiceException("没有找到对应记录");
        }
        //查询当前页记录
        int pageSize = 2;
        int startIndex = (pageCurrent-1)*pageSize;
        List<SysRole> records = sysRoleDao.findPageObjects(name, startIndex, pageSize);
        //封装查询结果并返回
        PageObject<SysRole> pageObject =
                new PageObject<>(pageCurrent,pageSize,rowCount,records);
        System.out.println(pageObject);
        return pageObject;
    }

    @Override
    public int deleteObject(Integer id) {
        //1.验证数据的安全性
        if(id==null||id<=0){
            throw new ServiceException("请先选择");
        }
        sysRoleMenuDao.deleteObjectsByRoleId(id);
        sysUserRoleDao.deleteObjectsByRoleId(id);
        int rows = sysRoleDao.deleteObject(id);
        if(rows==0){
            throw new ServiceException("此记录可能已经不存在");
        }
        return rows;
    }
}
