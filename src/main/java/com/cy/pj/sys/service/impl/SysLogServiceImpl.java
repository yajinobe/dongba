package com.cy.pj.sys.service.impl;

import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.vo.PageObject;
import com.cy.pj.sys.dao.SysLogDao;
import com.cy.pj.sys.entity.SysLog;
import com.cy.pj.sys.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @author yajinobe
 */
@Service
public class SysLogServiceImpl implements SysLogService {
    @Autowired
    private SysLogDao sysLogDao;
    @Override
    public PageObject<SysLog> findPageObjects(String name, Integer pageCurrent) {
        //1.验证参数和合法性
        if(pageCurrent==null||pageCurrent<1) {
            throw new IllegalArgumentException("当前页码不正确");
        }
        //2.基于条件查询总记录数 pageCount = (rowCount-1)/pageSize+1;
        int rowCount = sysLogDao.getRowCount(name);
        if(rowCount==0){
            //抛出ServiceException("系统没有查到对应记录")
            throw new ServiceException("系统没有查询到对应记录");
        }
        int pageSize = 2;
        int startIndex = (pageCurrent-1)*pageSize;
        List<SysLog> records = sysLogDao.findPageObjects(name, startIndex, pageSize);
        //3.对分页信息以及当前页记录进行封装
        PageObject<SysLog> pageObject = new PageObject<>();
        pageObject.setPageCount((rowCount-1)/pageSize+1).setPageCurrent(pageCurrent);
        pageObject.setPageSize(pageSize).setRowCount(rowCount).setRecords(records);
        return pageObject;
    }

    @Override
    public int deleteObjects(Integer[] ids) {
        if(ids==null||ids.length==0) {
            throw new IllegalArgumentException("请选择一个");
        }
        int rows;
        try {
            rows = sysLogDao.deleteObjects(ids);
        }catch (Throwable e) {
            throw new ServiceException("系统故障，正在恢复中");
        }
        if(rows==0){
            throw new ServiceException("记录可能已经不存在");
        }
        return rows;
    }

    @Override
    public void saveObject(SysLog sysLog) {
        sysLogDao.insertObject(sysLog);
    }
}
