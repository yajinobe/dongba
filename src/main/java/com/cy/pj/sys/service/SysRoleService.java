package com.cy.pj.sys.service;

import com.cy.pj.common.vo.PageObject;
import com.cy.pj.sys.entity.SysRole;

/**
 * @author yajinobe
 */
public interface SysRoleService {
    /**
     * 分页查询角色记录
     * @param name 角色名
     * @param pageCurrent 页码
     * @return 当前页记录
     */
    PageObject<SysRole> findPageObjects(String name,Integer pageCurrent);

    /**
     * 删除角色
     * @param id 角色id
     * @return 影响行数
     */
    int deleteObject(Integer id);
}
