package com.cy.pj.sys.service;

import com.cy.pj.common.vo.Node;
import com.cy.pj.sys.entity.SysMenu;

import java.util.List;
import java.util.Map;

/**
 * @author yajinobe
 */
public interface SysMenuService {
    /**
     * 查询菜单及其上级
     * @return 一个map封装一行记录
     */
    List<Map<String,Object>> findObjects();
    /**
     * 查询上级菜单相关信息
     */
    List<Node> findZtreeMenuNodes();
    /**
     * 删除菜单
     * @param id 菜单id
     * @return 影响行数
     */
    int deleteObject(Integer id);
    /**
     * 添加菜单
     * @param sysMenu 菜单对象
     * @return 影响行数
     */
    int saveObject(SysMenu sysMenu);

    /**
     * 更新菜单
     * @param sysMenu 菜单对象
     * @return 影响行数
     */
    int updateObject(SysMenu sysMenu);
}
